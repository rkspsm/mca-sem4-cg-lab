'use strict';

window.main = () => {

  const run = utils.firstByClass ('run');
  const clear = utils.firstByClass ('clear');
  const output = utils.firstByClass ('output');

  const x0Input = utils.firstByClass ('x0');
  const y0Input = utils.firstByClass ('y0');
  const x1Input = utils.firstByClass ('x1');
  const y1Input = utils.firstByClass ('y1');

  const getX0 = () => utils.defaultTo (5, parseInt (x0Input ().value));
  const getY0 = () => utils.defaultTo (10, parseInt (y0Input ().value));
  const getX1 = () => utils.defaultTo (20, parseInt (x1Input ().value));
  const getY1 = () => utils.defaultTo (30, parseInt (y1Input ().value));

  const messages = utils.firstByClass ('messages');

  const putStrLn = stuff => {
    utils.appendText (messages (), stuff);
    utils.appendHtml (messages (), '<br>');
    messages ().scrollTop = messages ().scrollHeight;
  };
  putStrLn ('Output :');

  const canvas = utils.Canvas (output (), 500, 500);
  putStrLn (`1 pixel is ${canvas.pixelSize}x${canvas.pixelSize} rectangle, with 0,0 being bottom left`);

  var controller = {cancel: utils.noop};

  const runClicked = () => {
    const x0 = getX0 ();
    const y0 = getY0 ();
    const x1 = getX1 ();
    const y1 = getY1 ();

    const dx = x1 - x0;
    const dy = y1 - y0;

    var steps;

    if (Math.abs (dx) > Math.abs (dy)) {
      steps = Math.abs (dx);
    } else {
      steps = Math.abs (dy);
    }

    if (steps == 0) {
      putStrLn ('steps = 0');
      return;
    }

    const xinc = dx / steps;
    const yinc = dy / steps;

    canvas.clear ();
    controller.cancel ();

    const tasks =
      [ () => putStrLn (`x0 = ${x0}`)
      , () => putStrLn (`y0 = ${y0}`)
      , () => putStrLn (`x1 = ${x1}`)
      , () => putStrLn (`y1 = ${y1}`)
      ];

    for (var v = 0; v <= steps; v++) {
      const x = parseInt (x0 + xinc * v);
      const y = parseInt (y0 + yinc * v);
      tasks.push (() => {
        putStrLn (`pixel : ${x}, ${y}`);
        canvas.pixelAt (x, y);
      });
    }

    tasks.push (() => run ().disabled = false);

    controller = utils.timed (500, tasks);

    run ().disabled = true;
  };

  const clearClicked = () => {
    canvas.clear ();
    controller.cancel ();
    run ().disabled = false;
  };

  run ().addEventListener ('click', runClicked);
  clear ().addEventListener ('click', clearClicked);

};

