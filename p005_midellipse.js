'use strict';

window.main = () => {

  const run = utils.firstByClass ('run');
  const clear = utils.firstByClass ('clear');
  const output = utils.firstByClass ('output');

  const xcInput = utils.firstByClass ('xc');
  const ycInput = utils.firstByClass ('yc');
  const rxInput = utils.firstByClass ('rx');
  const ryInput = utils.firstByClass ('ry');

  const getXc = () => utils.defaultTo (20, parseInt (xcInput ().value));
  const getYc = () => utils.defaultTo (30, parseInt (ycInput ().value));
  const getRx = () => utils.defaultTo (15, parseInt (rxInput ().value));
  const getRy = () => utils.defaultTo (5, parseInt (ryInput ().value));

  const messages = utils.firstByClass ('messages');

  const putStrLn = stuff => {
    utils.appendText (messages (), stuff);
    utils.appendHtml (messages (), '<br>');
    messages ().scrollTop = messages ().scrollHeight;
  };
  putStrLn ('Output :');

  const canvas = utils.Canvas (output (), 500, 500);
  putStrLn (`1 pixel is ${canvas.pixelSize}x${canvas.pixelSize} rectangle, with 0,0 being bottom left`);

  var controller = {cancel: utils.noop};

  const runClicked = () => {
    const xc = getXc ();
    const yc = getYc ();
    const a = getRx ();
    const b = getRy ();

    canvas.clear ();
    controller.cancel ();

    const tasks =
      [ () => putStrLn (`center-x = ${xc}`)
      , () => putStrLn (`center-y = ${yc}`)
      , () => putStrLn (`radius-x = ${a}`)
      , () => putStrLn (`radius-y = ${b}`)
      ];

    const drawPoint = (x, y) => tasks.push (() => {
      putStrLn (`pixel : ${x}, ${y}`);
      canvas.pixelAt (x, y);
    });

    const drawPoints = (x, y) => {
      const xs = [x, -x, x, -x];
      const ys = [y, y, -y, -y];
      for (var i = 0; i < 4; i++) {
        const tx = xc + xs[i];
        const ty = yc + ys[i];
        drawPoint (tx, ty);
      }
    };

    var a2 = a * a;
    var b2 = b * b;
    var twoa2 = 2 * a2;
    var twob2 = 2 * b2;
    var p;
    var x = 0;
    var y = b;
    var px = 0;
    var py = twoa2 * y;

    drawPoints (x, y);

    p = Math.round (b2 - (a2 * b) + (0.25 * a2));

    while (px < py) {
      x ++;
      px += twob2;
      if (p < 0) { p += b2 + px; }
      else {
        y --;
        py -= twoa2;
        p += b2 + px - py;
      }

      drawPoints (x, y);
    }

    p = Math.round (b2 * (x + 0.5) * (x + 0.5) + a2 * (y - 1) * (y - 1) - a2 * b2);

    while (y > 0) {
      y --;
      py -= twoa2;
      if (p > 0) { p += a2 - py; }
      else {
        x ++;
        px += twob2;
        p += a2 - py + px;
      }

      drawPoints (x, y);
    }

    tasks.push (() => run ().disabled = false);

    controller = utils.timed (150, tasks);

    run ().disabled = true;
  };

  const clearClicked = () => {
    canvas.clear ();
    controller.cancel ();
    run ().disabled = false;
  };

  run ().addEventListener ('click', runClicked);
  clear ().addEventListener ('click', clearClicked);

};

