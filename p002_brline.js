'use strict';

window.main = () => {

  const run = utils.firstByClass ('run');
  const clear = utils.firstByClass ('clear');
  const output = utils.firstByClass ('output');

  const x0Input = utils.firstByClass ('x0');
  const y0Input = utils.firstByClass ('y0');
  const x1Input = utils.firstByClass ('x1');
  const y1Input = utils.firstByClass ('y1');

  const getX0 = () => utils.defaultTo (5, parseInt (x0Input ().value));
  const getY0 = () => utils.defaultTo (10, parseInt (y0Input ().value));
  const getX1 = () => utils.defaultTo (20, parseInt (x1Input ().value));
  const getY1 = () => utils.defaultTo (30, parseInt (y1Input ().value));

  const messages = utils.firstByClass ('messages');

  const putStrLn = stuff => {
    utils.appendText (messages (), stuff);
    utils.appendHtml (messages (), '<br>');
    messages ().scrollTop = messages ().scrollHeight;
  };
  putStrLn ('Output :');

  const canvas = utils.Canvas (output (), 500, 500);
  putStrLn (`1 pixel is ${canvas.pixelSize}x${canvas.pixelSize} rectangle, with 0,0 being bottom left`);

  var controller = {cancel: utils.noop};

  const runClicked = () => {
    const x0 = getX0 ();
    const y0 = getY0 ();
    const x1 = getX1 ();
    const y1 = getY1 ();

    canvas.clear ();
    controller.cancel ();

    const tasks =
      [ () => putStrLn (`x0 = ${x0}`)
      , () => putStrLn (`y0 = ${y0}`)
      , () => putStrLn (`x1 = ${x1}`)
      , () => putStrLn (`y1 = ${y1}`)
      ];

    const dx = Math.abs (x1 - x0);
    const sx = x0 < x1 ? 1 : -1;
    const dy = Math.abs (y1 - y0);
    const sy = y0 < y1 ? 1 : -1;

    var _x = x0;
    var _y = y0;
    var err = (dx > dy ? dx : -dy) / 2;

    while (true) {
      const x = _x;
      const y = _y;
      tasks.push (() => {
        putStrLn (`pixel : ${x}, ${y}`);
        canvas.pixelAt (x, y);
      });

      if (_x === x1 && _y === y1) { break; }

      const e2 = err;
      if (e2 > -dx) { err -= dy; _x += sx; }
      if (e2 < dy) { err += dx; _y += sy; }
    }

    tasks.push (() => run ().disabled = false);

    controller = utils.timed (500, tasks);

    run ().disabled = true;
  };

  const clearClicked = () => {
    canvas.clear ();
    controller.cancel ();
    run ().disabled = false;
  };

  run ().addEventListener ('click', runClicked);
  clear ().addEventListener ('click', clearClicked);

};

