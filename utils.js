'use strict';

const utils = new Object ();

utils.noop = () => { };

utils.escapeHtml = text =>
  text
    .replace (/&/g, '&amp')
    .replace (/</g, '&lt')
    .replace (/>/g, '&lgt')
    .replace (/"/g, '&quote')

utils.appendHtml = (target, stuff) => {
  target.innerHTML += stuff
};

utils.appendText = (target, stuff) => {
  target.innerHTML += utils.escapeHtml (stuff);
};

utils.defaultTo = (d, x) => {
  if (typeof x === "undefined") { return d; }
  if (x === null) { return d; }
  if (x.toString () === "NaN") { return d; }
  return x;
};

utils.clamp = (_lower, _upper, _x) => {
  const x = utils.defaultTo (0, _x);
  const lower = utils.defaultTo (0, _lower);
  const upper = utils.defaultTo (1, _upper);

  if (x < lower) { return lower; }
  if (x > upper) { return upper; }
  return x;
};

utils.clampDefault = (d, l, u, x) =>
  utils.clamp (l, u, utils.defaultTo (d, x));

utils.firstByClass = name =>
  () => utils.defaultTo (null, document.getElementsByClassName (name)[0]);

utils.Canvas = (element, width, height) => {
  const obj = new Object ();

  obj.element = element;
  obj.width = width;
  obj.height = height;

  const ctx = obj.ctx = element.getContext ("2d");

  ctx.fillStyle = "black";

  const clear = obj.clear = () => {
    ctx.fillStyle = "white";
    ctx.fillRect (0, 0, width, height);
    ctx.fillStyle = "black";
  };

  const pixelSize = obj.pixelSize = 5;

  const pixelAt = obj.pixelAt = (_x, _y) => {
    const x = utils.clampDefault (0, 0, width / pixelSize, _x);
    const y = utils.clampDefault (0, 0, height / pixelSize, _y);
    ctx.fillRect (pixelSize * x, height - pixelSize * y, pixelSize, pixelSize);
  }

  return obj;
};

utils._timed = (interval, tasks, controller) => {
  if (tasks.length < 1 || controller.stop == true) { return; }
  else {
    const task = tasks[0];
    const rest = tasks.slice (1);
    const stId = setTimeout (() => {
      task ();
      utils._timed (interval, rest, controller);
    }, interval);
    controller.clearTimeout = () => clearTimeout (stId);
  }
};

utils.timed = (interval, tasks) => {
  const controller = new Object ();
  controller.stop = false;
  controller.clearTimeout = utils.noop;
  const cancel = () => { controller.clearTimeout (); controller.stop = true; };
  utils._timed (interval, tasks, controller);
  return {cancel};
};

window.addEventListener ('load', () => { window.main (); });

