'use strict';

window.main = () => {

  const mainContainer = utils.firstByClass ('main');

  const addContent = content => utils.appendHtml (mainContainer (), content);

  const addTitle = text =>
    addContent (`<div style='font-weight:bold'>${utils.escapeHtml (text)}</div>`);

  const addLink = (label, file) =>
    addContent (`<div style='padding-left:2rem'><a href=${file}>${utils.escapeHtml (label)}</a></div>`);

  addTitle ('CG Programs');
  addLink ('1. Line using DDA', 'p001_dda.html');
  addLink ('2. Line using Bresenham\'s', 'p002_brline.html');
  addLink ('3. Circle using Bresenham\'s', 'p003_brcircle.html');
  addLink ('4. Circle using Mid-Point', 'p004_midcircle.html');
  addLink ('5. Ellipse using Mid-Point', 'p005_midellipse.html');

};

