'use strict';

window.main = () => {

  const run = utils.firstByClass ('run');
  const clear = utils.firstByClass ('clear');
  const output = utils.firstByClass ('output');

  const xcInput = utils.firstByClass ('xc');
  const ycInput = utils.firstByClass ('yc');
  const rInput = utils.firstByClass ('r');

  const getXc = () => utils.defaultTo (20, parseInt (xcInput ().value));
  const getYc = () => utils.defaultTo (30, parseInt (ycInput ().value));
  const getR = () => utils.defaultTo (15, parseInt (rInput ().value));

  const messages = utils.firstByClass ('messages');

  const putStrLn = stuff => {
    utils.appendText (messages (), stuff);
    utils.appendHtml (messages (), '<br>');
    messages ().scrollTop = messages ().scrollHeight;
  };
  putStrLn ('Output :');

  const canvas = utils.Canvas (output (), 500, 500);
  putStrLn (`1 pixel is ${canvas.pixelSize}x${canvas.pixelSize} rectangle, with 0,0 being bottom left`);

  var controller = {cancel: utils.noop};

  const runClicked = () => {
    const xc = getXc ();
    const yc = getYc ();
    const r = getR ();

    canvas.clear ();
    controller.cancel ();

    const tasks =
      [ () => putStrLn (`center-x = ${xc}`)
      , () => putStrLn (`center-y = ${yc}`)
      , () => putStrLn (`radius = ${r}`)
      ];


    const drawPoint = (x, y) => tasks.push (() => {
      putStrLn (`pixel : ${x}, ${y}`);
      canvas.pixelAt (x, y);
    });

    const drawPoints = (x, y) => {
      const xs = [x, -x, x, -x, y, -y, y, -y];
      const ys = [y, y, -y, -y, x, x, -x, -x];
      for (var i = 0; i < 8; i++) {
        const tx = xc + xs[i];
        const ty = yc + ys[i];
        drawPoint (tx, ty);
      }
    };

    var f = 1 - r;
    var ddf_x = 1;
    var ddf_y = -2 * r;
    var x = 0;
    var y = r;

    drawPoint (xc, yc + r);
    drawPoint (xc, yc - r);
    drawPoint (xc + r, yc);
    drawPoint (xc - r, yc);

    while (x < y) {
      if (f >= 0) {
        y -= 1;
        ddf_y += 2;
        f += ddf_y;
      }

      x += 1;
      ddf_x += 2;
      f += ddf_x;

      drawPoints (x, y);
    }

    tasks.push (() => run ().disabled = false);

    controller = utils.timed (150, tasks);

    run ().disabled = true;
  };

  const clearClicked = () => {
    canvas.clear ();
    controller.cancel ();
    run ().disabled = false;
  };

  run ().addEventListener ('click', runClicked);
  clear ().addEventListener ('click', clearClicked);

};

